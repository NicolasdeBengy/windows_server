# TP Windows Server

## I. Mise en place du partage

* Création du dossier et ajout des droits à un utilisateur:

![](https://i.imgur.com/ZYgmVTm.png)


* Accès au partage depuis le client:

![](https://i.imgur.com/W41k0OQ.png)

## II. Mise en place du DHCP

* Ajout d'une étendue de 10.0.2.20/24 à 10.0.2.40/24:

![](https://i.imgur.com/KGGbegs.png)

* Vérification du fonctionnement du DHCP:

*Client:* 

![](https://i.imgur.com/jKDPpoy.png)

*Serveur:* 

![](https://i.imgur.com/fiQXl8q.png)

### III. Mise en place du DNS

* Création d'un alias et d'une étendue directe inversée:

![](https://i.imgur.com/QoK73NL.png)

* Vérification du fonctionnement du DNS:

![](https://i.imgur.com/kUFmQM6.png)

## IV. Mise en place du GPO

* Création d'un dossier partagé sur le bureau dans l'unité d'organisation DSIA et administré par le groupe grp-b2A créé préalablement contenant une image:

![](https://i.imgur.com/IHZgrW6.png)

* Création d'une nouvelle stratégie de groupe pour l'unité d'organisation DSIA:

![](https://i.imgur.com/7VJUiWP.png)

* Automatisation de la géstion des fonds d'écrans du groupe DSIA:

![](https://i.imgur.com/UkAdC7h.png)

* Vérification du bon fonctionnement depuis le client:

![](https://i.imgur.com/CVAZ6Cs.png)

## V. Travail Individuel

## 2.

### a
* Configuration d'un deuxième serveur avec DNS & ADDS:

![](https://i.imgur.com/DfvZHcr.png)

* Ajout d'une forêt et configuration du DNS:

![](https://i.imgur.com/wC0ZV4B.png)

### b 

* Vérification du bon fonctionnement du DNS:

*Serveur 1:*

![](https://i.imgur.com/8Nbt3Vx.png)

*Serveur 2:*

![](https://i.imgur.com/zxRsuAu.png)

### c

* Configuration de deux groupes de sécurité universelle:

*Groupe 1:*

![](https://i.imgur.com/86RPUzI.png)

*Groupe 2:*

![](https://i.imgur.com/C1b6FDg.png)

### d

* Ajout des groupes à leur groupe  administrateurs:

*Groupe 1:*

![](https://i.imgur.com/94vVuF1.png)

*Groupe 2:*

![](https://i.imgur.com/pKoJmwZ.png)

### e

* Création des comptes administrateurs:

*Domaine 1:*

![](https://i.imgur.com/mRLkfoL.png)


*Domaine 2:*

![](https://i.imgur.com/5vcBARA.png)


* Ajout des comptes administrateurs aux groupes universelles:

*Domaine 1:*

![](https://i.imgur.com/qHo9CfV.png)


*Domaine 2:*

![](https://i.imgur.com/H0GKQ7T.png)


### f

* Connexion au compte nicoladmin et création des redirecteurs conditionnels:

![](https://i.imgur.com/pYc6XLO.png)


## 4. Validation du fonctionnement réseau

### a

* Ping entre les deux serveurs:

![](https://i.imgur.com/268O17p.png)

### b

* NSLookup:

*Serveur 1:*

![](https://i.imgur.com/LmeBSiZ.png)

*Serveur 2:*

![](https://i.imgur.com/Ob3eswV.png)

## 5. Création de la relation d'approbation inter-forêt bidirectionnelle, transitive sur les Domain Controller

![](https://i.imgur.com/6e8vmdA.png)

![](https://i.imgur.com/v4SunoE.png)

## 6. Validation des relations d'approbation entrantes et sortantes.
![](https://i.imgur.com/DshVqk9.png)

## 7. Validation du bon fonctionnement de la relation d'approbation via Utilisateurs et Ordinateurs Active Directory
![](https://i.imgur.com/vDIfDc1.png)
